package br.com.htmind.yourecipe.module.contentful.model


data class EntryCollection(
    val total: Int, val skip: Int, val limit: Int,
    val items: List<Entry>,  val includes: Includes? = null) {

    fun getEntryFromLink(linkEntity: Map<String, Any>?): Entry? {

        if(linkEntity == null) return null

        val sys = linkEntity["sys"] as? Map<String, Any>

        if(sys?.get("linkType") != "Entry") return null

        val id = sys.get("id") as? String

        return includes?.entries?.firstOrNull { entry -> entry.sys.id == id }
    }

    fun getAssetFromLink(linkEntity: Map<String, Any>?): Entry? {

        if(linkEntity == null) return null

        val sys = linkEntity["sys"] as? Map<String, Any>

        if(sys?.get("linkType") != "Asset") return null

        val id = sys.get("id") as? String

        return includes?.assets?.firstOrNull { entry -> entry.sys.id == id }
    }
}
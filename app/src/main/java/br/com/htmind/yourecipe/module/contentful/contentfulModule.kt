package br.com.htmind.yourecipe.module.contentful

import br.com.htmind.yourecipe.module.contentful.network.ContentfulApi
import br.com.htmind.yourecipe.module.contentful.network.ContentfulNetworkRepository
import br.com.htmind.yourecipe.module.contentful.network.ContentfulRepository
import br.com.htmind.yourecipe.module.contentful.service.ContentfulChefService
import br.com.htmind.yourecipe.module.contentful.service.ContentfulRecipeService
import br.com.htmind.yourecipe.module.contentful.service.ContentfulTagService
import br.com.htmind.yourecipe.service.ChefService
import br.com.htmind.yourecipe.service.RecipeService
import br.com.htmind.yourecipe.service.TagService
import br.com.htmind.yourecipe.util.ParamsResolver
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

val contentfulModule = module {

    single<Retrofit> {
        val metaDataResolver: ParamsResolver = get()
        val endpoint = metaDataResolver.getString("br.com.htmind.yourecipe.endpoint")
        Retrofit.Builder().baseUrl(endpoint)
            .addConverterFactory(MoshiConverterFactory.create())
        .build()
    }

    single<ContentfulApi> {
        val retrofit: Retrofit = get()
        retrofit.create(ContentfulApi::class.java)
    }

    single<ContentfulNetworkRepository> {
        ContentfulRepository(get(), get())
    }

    single<ChefService> {
        ContentfulChefService(get())
    }

    single<TagService> {
        ContentfulTagService(get())
    }

    single<RecipeService> {
        ContentfulRecipeService(get())
    }
}


package br.com.htmind.yourecipe.content


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.htmind.yourecipe.R
import br.com.htmind.yourecipe.model.Photo
import br.com.htmind.yourecipe.model.Recipe
import br.com.htmind.yourecipe.vm.RecipeListViewModel
import com.squareup.picasso.Picasso
import org.koin.android.viewmodel.ext.android.viewModel


class RecipeListFragment : Fragment() {

    private var recipes: Array<Recipe>? = null

    private val recipeListViewModel: RecipeListViewModel by viewModel()

    private val recyclerView: RecyclerView? get() = view?.findViewById<RecyclerView >(R.id.recyclerView)

    private var adapter: ViewAdapter? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_recipe_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (adapter == null) {
            recipeListViewModel.recipes.observe(this, Observer { result ->
                recipes = result
                adapter = ViewAdapter(result) { onItemSelected(it) }
                displayData()
            })
        } else {
            displayData()
        }
    }

    private fun displayData() {
        recyclerView?.adapter = adapter
        recyclerView?.layoutManager = LinearLayoutManager(activity!!)
        adapter?.notifyDataSetChanged()
    }

    private fun onItemSelected(position: Int) {

        val recipeId = recipes?.get(position)?.id ?: ""
        findNavController().navigate(R.id.openRecipeViewFragment, bundleOf("recipeId" to recipeId))
    }

    class ViewAdapter(val items: Array<Recipe>, private val listener: (Int) -> Unit): RecyclerView.Adapter<ViewAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_recipe_item, parent, false))

        override fun getItemCount() = items.size

        override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], position, listener)

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            fun bind(item: Recipe, pos: Int, listener: (Int) -> Unit) = with(itemView) {

                findViewById<TextView>(R.id.textViewTitle).run {
                    this.text = item.title
                }

                findViewById<TextView>(R.id.textViewSubtitle).run {
                    this.text = item.chef?.name ?: ""
                }

                findViewById<CardView>(R.id.cardView).run {
                    this.setOnClickListener {
                        listener(pos)
                    }
                }

                findViewById<ImageView>(R.id.imageViewHero).run {
                    when(item.photo) {
                        is Photo -> Picasso.get()
                            .load(item.photo.url)
                            .into(this)
                    }
                }
            }
        }
    }
}

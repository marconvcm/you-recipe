package br.com.htmind.yourecipe.module

import br.com.htmind.yourecipe.vm.RecipeListViewModel
import br.com.htmind.yourecipe.vm.RecipeViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module


val appModule = module {

    viewModel { RecipeViewModel(get()) }

    viewModel { RecipeListViewModel(get(), get(), get()) }
}
package br.com.htmind.yourecipe.module.contentful_sdk

import br.com.htmind.yourecipe.module.contentful_sdk.service.ContentfulSdkChefService
import br.com.htmind.yourecipe.module.contentful_sdk.service.ContentfulSdkRecipeService
import br.com.htmind.yourecipe.module.contentful_sdk.service.ContentfulSdkTagService
import br.com.htmind.yourecipe.service.ChefService
import br.com.htmind.yourecipe.service.RecipeService
import br.com.htmind.yourecipe.service.TagService
import br.com.htmind.yourecipe.util.ParamsResolver
import com.contentful.java.cda.CDAClient
import org.koin.dsl.module

val contentfulSdkModule = module {

    single<CDAClient> {
        val params: ParamsResolver = get()
        CDAClient.builder().apply {
            setSpace(params.getString("br.com.htmind.yourecipe.space_id"))
            setToken(params.getString("br.com.htmind.yourecipe.access_token"))
        }.build()
    }

    single<ChefService> {
        ContentfulSdkChefService(get())
    }

    single<TagService> {
        ContentfulSdkTagService(get())
    }

    single<RecipeService> {
        ContentfulSdkRecipeService(get())
    }
}
package br.com.htmind.yourecipe.service

import br.com.htmind.yourecipe.model.Chef

interface ChefService {

    suspend fun getAll(): Array<Chef>
}
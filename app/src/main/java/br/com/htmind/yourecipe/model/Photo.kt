package br.com.htmind.yourecipe.model

data class Photo (
    val url: String,
    val width: Int,
    val height: Int
)
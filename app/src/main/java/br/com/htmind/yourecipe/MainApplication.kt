package br.com.htmind.yourecipe

import android.app.Application
import br.com.htmind.yourecipe.module.appModule
import br.com.htmind.yourecipe.module.contentful.contentfulModule
import br.com.htmind.yourecipe.module.contentful_sdk.contentfulSdkModule
import br.com.htmind.yourecipe.module.mainModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@MainApplication)
            modules(mainModule)
            modules(contentfulSdkModule)
            //modules(contentfulModule)
            //modules(inmemoryModule)
            modules(appModule)
        }
    }
}


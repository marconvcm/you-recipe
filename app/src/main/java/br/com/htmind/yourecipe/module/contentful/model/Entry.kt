package br.com.htmind.yourecipe.module.contentful.model

data class Entry (
    val sys: EntryMetadata,
    val fields: Map<String, Any>
)

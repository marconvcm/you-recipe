package br.com.htmind.yourecipe.module.inmemory.service

import br.com.htmind.yourecipe.model.Photo
import br.com.htmind.yourecipe.model.Recipe
import br.com.htmind.yourecipe.service.ChefService
import br.com.htmind.yourecipe.service.RecipeService
import br.com.htmind.yourecipe.service.TagService

class InMemoryRecipeService(val tagService: TagService, val chefService: ChefService):
    RecipeService {

    private suspend fun dataSource() = arrayOf(
        Recipe(
            id = "RC1",
            title = "On-the-Farm Scrambled Eggs",
            photo = Photo(
                url = "https://images.media-allrecipes.com/userphotos/560x315/4521371.jpg",
                width = 100,
                height = 100
            ),
            calories = "270 calories",
            description = "<h1>Creamy scrambled eggs with chives and goat cheese create an earthy flavor that will bring you to the farm. </h1> A dash of sriracha is your bus ticket home if you need it. Serve with your favorite toast, and since you are there, a slice or two of bacon.",
            chef = chefService.getAll().last(),
            tags = tagService.getAll().sliceArray(IntRange(0, 1))
        )
    )



    override suspend fun getById(recipeId: String): Recipe? {
        return dataSource().first { it.id == recipeId }
    }

    override suspend fun getAll(): Array<Recipe> {
        return dataSource()
    }
}

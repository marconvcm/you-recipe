package br.com.htmind.yourecipe.content


import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import br.com.htmind.yourecipe.R
import br.com.htmind.yourecipe.model.Photo
import br.com.htmind.yourecipe.vm.RecipeViewModel
import com.squareup.picasso.Picasso
import org.koin.android.viewmodel.ext.android.viewModel

class RecipeViewFragment : Fragment() {

    private val recipeViewModel: RecipeViewModel by viewModel()

    private val recipeId: String get() = (arguments?.getString("recipeId") ?: "")

    private val textViewTitle get () = view?.findViewById<TextView>(R.id.textViewTitle)

    private val textViewSubtitle get () = view?.findViewById<TextView>(R.id.textViewSubtitle)

    private val textViewDescription get () = view?.findViewById<TextView>(R.id.textViewDescription)

    private val imageViewHero get () = view?.findViewById<ImageView>(R.id.imageViewHero)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_recipe_view, container, false)

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recipeViewModel.load(recipeId)

        recipeViewModel.recipe.observe(this, Observer { recipe ->

            textViewTitle?.text = recipe.title

            textViewSubtitle?.text = recipe.chef?.name ?: ""

            textViewDescription?.text = Html.fromHtml(recipe.description, Html.FROM_HTML_MODE_COMPACT)

            when(recipe.photo) {
                is Photo -> Picasso.get().load(recipe.photo.url).into(imageViewHero)
            }
        })
    }
}

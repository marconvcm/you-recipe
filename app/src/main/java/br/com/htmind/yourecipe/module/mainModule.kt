package br.com.htmind.yourecipe.module

import br.com.htmind.yourecipe.util.MetaDataResolver
import br.com.htmind.yourecipe.util.ParamsResolver
import org.koin.dsl.module

val mainModule = module {

    single<ParamsResolver> { MetaDataResolver(get()) }
}
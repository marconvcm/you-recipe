package br.com.htmind.yourecipe.module.contentful.model

import com.squareup.moshi.Json

data class Includes (

    @field:Json(name = "Entry")
    val entries: List<Entry>,

    @field:Json(name = "Asset")
    val assets: List<Entry>
)
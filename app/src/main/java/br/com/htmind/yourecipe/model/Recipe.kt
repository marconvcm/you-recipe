package br.com.htmind.yourecipe.model

data class Recipe (
    val id: String,
    val title: String,
    val photo: Photo?,
    val calories: String,
    val description: String,
    val chef: Chef?,
    val tags: Array<Tag>
)
package br.com.htmind.yourecipe.module.contentful.model

data class EntryMetadata (
    val id: String,
    val type: String,
    val createdAt: String,
    val updatedAt: String,
    val revision: Int
)

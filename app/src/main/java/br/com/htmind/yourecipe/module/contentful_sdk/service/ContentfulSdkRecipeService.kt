package br.com.htmind.yourecipe.module.contentful_sdk.service

import br.com.htmind.yourecipe.model.Photo
import br.com.htmind.yourecipe.model.Recipe
import br.com.htmind.yourecipe.service.RecipeService
import com.contentful.java.cda.CDAAsset
import com.contentful.java.cda.CDAClient
import com.contentful.java.cda.CDAEntry
import com.contentful.java.cda.image.ImageOption

class ContentfulSdkRecipeService(private val client: CDAClient) : RecipeService {

    override suspend fun getAll(): Array<Recipe> {
        return client.fetch(CDAEntry::class.java).where("content_type", "recipe").all().items()
            .map { it as CDAEntry }
            .map { parseEntryToModel(it) }.toTypedArray()
    }

    override suspend fun getById(recipeId: String): Recipe? =
        parseEntryToModel(client.fetch(CDAEntry::class.java).one(recipeId))


    companion object {
        fun parseEntryToModel(entry: CDAEntry): Recipe = Recipe(
            id = entry.id(),
            title = entry.getField("title"),
            calories = "" + entry.getField("calories"),
            chef = entry.getField<CDAEntry>("chef").takeIf { it != null }?.let { chef ->
                ContentfulSdkChefService.parseEntryToModel(chef)
            },
            description = entry.getField("description"),
            photo = (entry.getField("photo") as CDAAsset).let {
                Photo(
                    url = it.urlForImageWith(ImageOption.https()),
                    width =  0,
                    height = 0
                )
            },
            tags = entry.getField<List<CDAEntry>>("tags")
                .takeIf { it != null }
                ?.map { ContentfulSdkTagService.parseEntryToModel(it) }
                ?.toTypedArray() ?: emptyArray()
        )
    }
}
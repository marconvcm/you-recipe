package br.com.htmind.yourecipe.module.contentful_sdk.service

import br.com.htmind.yourecipe.model.Chef
import br.com.htmind.yourecipe.service.ChefService
import com.contentful.java.cda.CDAClient
import com.contentful.java.cda.CDAEntry

class ContentfulSdkChefService (val client: CDAClient) : ChefService {

    override suspend fun getAll(): Array<Chef> {
        return client.fetch(CDAEntry::class.java).where("content_type", "chef").all().items()
            .map { it as CDAEntry }
            .map { parseEntryToModel(it) }.toTypedArray()
    }

    companion object {
        fun parseEntryToModel(entry: CDAEntry): Chef = Chef (
            id = entry.id(),
            name = entry.getField("name")
        )
    }
}


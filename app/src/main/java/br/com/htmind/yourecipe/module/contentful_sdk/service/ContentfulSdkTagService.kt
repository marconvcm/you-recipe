package br.com.htmind.yourecipe.module.contentful_sdk.service

import br.com.htmind.yourecipe.model.Tag
import br.com.htmind.yourecipe.service.TagService
import com.contentful.java.cda.CDAClient
import com.contentful.java.cda.CDAEntry

class ContentfulSdkTagService(val client: CDAClient)  : TagService {

    override suspend fun getAll(): Array<Tag> {
        return client.fetch(CDAEntry::class.java).where("content_type", "tag").all().items()
            .map { it as CDAEntry }
            .map { parseEntryToModel(it) }.toTypedArray()
    }

    companion object {
        fun parseEntryToModel(entry: CDAEntry): Tag = Tag (
            id = entry.id(),
            name = entry.getField("name")
        )
    }
}
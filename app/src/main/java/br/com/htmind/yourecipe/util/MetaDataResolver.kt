package br.com.htmind.yourecipe.util

import android.content.Context
import android.content.pm.PackageManager


interface ParamsResolver {

    fun getString(key: String): String
}

class MetaDataResolver (val context: Context) : ParamsResolver {

    private val info get() = context.packageManager.getApplicationInfo(context.packageName, PackageManager.GET_META_DATA)

    override fun getString(key: String) = info.metaData.getString(key)!!
}
package br.com.htmind.yourecipe.vm

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.htmind.yourecipe.model.Chef
import br.com.htmind.yourecipe.model.Recipe
import br.com.htmind.yourecipe.model.Tag
import br.com.htmind.yourecipe.service.ChefService
import br.com.htmind.yourecipe.service.RecipeService
import br.com.htmind.yourecipe.service.TagService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RecipeListViewModel(val recipeService: RecipeService, val chefService: ChefService, val tagService: TagService): ViewModel() {

    var recipes = MutableLiveData<Array<Recipe>>()
    var tags = MutableLiveData<Array<Tag>>()
    var chefs = MutableLiveData<Array<Chef>>()

    init {
        viewModelScope.launch {
            recipes.postValue(getRecipes())
            tags.postValue(getTags())
            chefs.postValue(getChefs())
        }
    }

    private suspend fun getRecipes() = withContext(Dispatchers.IO) { recipeService.getAll() }

    private suspend fun getChefs() = withContext(Dispatchers.IO) { chefService.getAll() }

    private suspend fun getTags() = withContext(Dispatchers.IO) { tagService.getAll() }
}
package br.com.htmind.yourecipe.module.contentful.network

import br.com.htmind.yourecipe.module.contentful.model.Entry
import br.com.htmind.yourecipe.module.contentful.model.EntryCollection
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ContentfulApi {

    @GET("/spaces/{space_id}/environments/{environment_id}/entries")
    suspend fun query(
        @Path("space_id") spaceId: String,
        @Path("environment_id") environmentId: String,
        @Query("access_token") accessToken: String,
        @Query("content_type") contentType: String
    ): Response<EntryCollection>

    @GET("/spaces/{space_id}/environments/{environment_id}/entries/{entry_id}")
    suspend fun queryEntry(
        @Path("space_id") spaceId: String,
        @Path("environment_id") environmentId: String,
        @Path("entry_id") entryId: String,
        @Query("access_token") accessToken: String
    ): Response<Entry>
}


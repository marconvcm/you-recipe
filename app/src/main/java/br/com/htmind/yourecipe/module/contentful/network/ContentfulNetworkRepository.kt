package br.com.htmind.yourecipe.module.contentful.network

import br.com.htmind.yourecipe.module.contentful.model.Entry
import br.com.htmind.yourecipe.module.contentful.model.EntryCollection
import retrofit2.Response

interface ContentfulNetworkRepository {

    suspend fun query(contentType: String): Response<EntryCollection>

    suspend fun queryEntry(entryId: String): Response<Entry>
}
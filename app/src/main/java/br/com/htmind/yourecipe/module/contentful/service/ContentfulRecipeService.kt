package br.com.htmind.yourecipe.module.contentful.service

import br.com.htmind.yourecipe.model.Chef
import br.com.htmind.yourecipe.model.Photo
import br.com.htmind.yourecipe.model.Recipe
import br.com.htmind.yourecipe.model.Tag
import br.com.htmind.yourecipe.module.contentful.network.ContentfulNetworkRepository
import br.com.htmind.yourecipe.service.RecipeService

class ContentfulRecipeService (val repository: ContentfulNetworkRepository): RecipeService {

    override suspend fun getById(recipeId: String): Recipe? {
        return getAll().find { it.id == recipeId }
    }

    override suspend fun getAll(): Array<Recipe> {

        val response = repository.query("recipe")

        val collection = response.body().takeIf { it != null } ?: return emptyArray()

        return collection.items.map { entry ->
            Recipe(
                id = entry.sys.id,
                title = entry.fields["title"] as String ?: "",
                description = entry.fields["description"] as String ?: "",
                calories = entry.fields["calories"].let {
                    when(it) {
                        is Int -> "${it}"
                        else -> ""
                    }
                },
                photo = collection.getAssetFromLink(entry.fields["photo"] as? Map<String, Any>)?.let { item ->
                    val file = item?.fields?.get("file") as? Map<String, Any>
                    val details = file?.get("details") as? Map<String, Any>
                    val image = details?.get("image") as? Map<String, Any>
                    val url = file?.get("url") as String
                    Photo(
                        url = "https:${url}",
                        width = (image?.get("width") as Double).toInt(),
                        height = (image?.get("height") as Double).toInt()
                    )
                },
                chef = collection.getEntryFromLink(entry.fields["chef"] as? Map<String, Any>)?.let { item ->
                    Chef(
                        id = item?.sys?.id ?: "",
                        name = item?.fields?.get("name") as? String ?: ""
                    )
                },
                tags = (entry.fields["tags"] as? Array<Map<String, Any>>)?.map { tag -> collection.getEntryFromLink(tag) }?.map { tagEntry ->
                    Tag(
                        id = tagEntry?.sys?.id ?: "",
                        name = tagEntry?.fields?.get("name") as? String ?: ""
                    )
                }?.toTypedArray() ?: emptyArray()
            )
        }.toTypedArray()
    }
}

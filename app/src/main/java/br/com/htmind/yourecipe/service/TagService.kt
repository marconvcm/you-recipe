package br.com.htmind.yourecipe.service

import br.com.htmind.yourecipe.model.Tag

interface TagService {

    suspend fun getAll(): Array<Tag>
}
package br.com.htmind.yourecipe.vm

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.htmind.yourecipe.model.Recipe
import br.com.htmind.yourecipe.service.RecipeService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RecipeViewModel(val recipeService: RecipeService): ViewModel() {

    val recipe = MutableLiveData<Recipe>()

    fun load(recipeId: String) {
        viewModelScope.launch {
            val result = getById(recipeId)
            recipe.postValue(result)
        }
    }

    private suspend fun getById(recipeId: String) = withContext(Dispatchers.IO) { recipeService.getById(recipeId) }
}
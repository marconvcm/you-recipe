package br.com.htmind.yourecipe.module.contentful.network

import br.com.htmind.yourecipe.util.ParamsResolver

open class ContentfulRepository (val api: ContentfulApi, val paramsResolver: ParamsResolver): ContentfulNetworkRepository {

    private val spaceId by lazy { paramsResolver.getString("br.com.htmind.yourecipe.space_id") }

    private val accessToken by lazy { paramsResolver.getString("br.com.htmind.yourecipe.access_token") }

    private val environmentId by lazy { paramsResolver.getString("br.com.htmind.yourecipe.environment_id") }

    override suspend fun query(contentType: String) = api.query(spaceId = spaceId, environmentId = environmentId, accessToken = accessToken, contentType = contentType)

    override suspend fun queryEntry(entryId: String) = api.queryEntry(spaceId = spaceId, environmentId = environmentId, accessToken = accessToken, entryId = entryId)
}
package br.com.htmind.yourecipe.module.inmemory.service

import br.com.htmind.yourecipe.model.Tag
import br.com.htmind.yourecipe.service.TagService

class InMemoryTagService: TagService {

    private val dataSource by lazy {
        arrayOf(
            Tag(id = "1", name = "vegan"),
            Tag(id = "2", name = "vegetarian"),
            Tag(id = "3", name = "low fat"),
            Tag(id = "4", name = "keto")
        )
    }

    override suspend fun getAll(): Array<Tag> {
        return dataSource;
    }
}

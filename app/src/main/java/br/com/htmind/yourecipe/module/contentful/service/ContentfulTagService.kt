package br.com.htmind.yourecipe.module.contentful.service

import br.com.htmind.yourecipe.model.Tag
import br.com.htmind.yourecipe.module.contentful.network.ContentfulNetworkRepository
import br.com.htmind.yourecipe.service.TagService

class ContentfulTagService (val repository: ContentfulNetworkRepository): TagService {

    override suspend fun getAll(): Array<Tag> {

        val response = repository.query("tag")

        val collection = response.body().takeIf { it != null } ?: return emptyArray()

        return collection.items.map { entry ->
            Tag(
                id = entry.sys.id,
                name = entry.fields["name"] as String ?: ""
            )
        }.toTypedArray()
    }
}

package br.com.htmind.yourecipe.module.inmemory.service

import br.com.htmind.yourecipe.model.Chef
import br.com.htmind.yourecipe.service.ChefService

internal class InMemoryChefService: ChefService {

    private val dataSource by lazy {
        arrayOf(
            Chef(id = "1", name = "Marcos"),
            Chef(id = "2", name = "Willian"),
            Chef(id = "3", name = "Fogaca"),
            Chef(id = "4", name = "Eric Jackin")
        )
    }

    override suspend fun getAll(): Array<Chef> {
        return dataSource;
    }
}

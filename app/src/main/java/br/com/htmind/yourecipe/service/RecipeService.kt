package br.com.htmind.yourecipe.service

import br.com.htmind.yourecipe.model.Recipe

interface RecipeService {

    suspend fun getAll(): Array<Recipe>

    suspend fun getById(recipeId: String): Recipe?
}
package br.com.htmind.yourecipe.module.contentful.service

import br.com.htmind.yourecipe.model.Chef
import br.com.htmind.yourecipe.module.contentful.network.ContentfulNetworkRepository
import br.com.htmind.yourecipe.service.ChefService

class ContentfulChefService (val repository: ContentfulNetworkRepository) : ChefService {

    override suspend fun getAll(): Array<Chef> {

        val response = repository.query("chef")

        val collection = response.body().takeIf { it != null } ?: return emptyArray()

        return collection.items.map { entry ->
            Chef(
                id = entry.sys.id,
                name = entry.fields["name"] as String ?: ""
            )
        }.toTypedArray()
    }
}

package br.com.htmind.yourecipe.model

data class Tag (
    val id: String,
    val name: String
)
package br.com.htmind.yourecipe.module.inmemory

import br.com.htmind.yourecipe.module.inmemory.service.InMemoryChefService
import br.com.htmind.yourecipe.module.inmemory.service.InMemoryRecipeService
import br.com.htmind.yourecipe.module.inmemory.service.InMemoryTagService
import br.com.htmind.yourecipe.service.ChefService
import br.com.htmind.yourecipe.service.RecipeService
import br.com.htmind.yourecipe.service.TagService
import org.koin.dsl.module

val inmemoryModule = module {

    single<ChefService> { InMemoryChefService() }
    single<TagService> { InMemoryTagService() }
    single<RecipeService> { InMemoryRecipeService(get(), get()) }
}


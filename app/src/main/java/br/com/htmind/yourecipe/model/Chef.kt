package br.com.htmind.yourecipe.model

data class Chef (
    val id: String,
    val name: String
)
package br.com.htmind.yourecipe.module.contentful.service

import br.com.htmind.yourecipe.model.Chef
import br.com.htmind.yourecipe.model.Tag
import br.com.htmind.yourecipe.module.contentful.model.Entry
import br.com.htmind.yourecipe.module.contentful.model.EntryCollection
import br.com.htmind.yourecipe.module.contentful.model.EntryMetadata
import br.com.htmind.yourecipe.module.contentful.network.ContentfulNetworkRepository
import br.com.htmind.yourecipe.module.contentful.network.ContentfulRepository
import br.com.htmind.yourecipe.service.ChefService
import br.com.htmind.yourecipe.service.TagService
import kotlinx.coroutines.runBlocking
import org.junit.Test

import org.junit.Assert.*
import org.mockito.Mockito
import retrofit2.Response

class ContentfulChefServiceTest {

    val entryCollection = EntryCollection(
        total = 1,
        skip = 0,
        limit = 10,
        items = listOf(
            Entry(
                sys = EntryMetadata(
                    id = "C1",
                    createdAt = "123",
                    updatedAt = "123",
                    type = "chef",
                    revision = 1
                ),
                fields = mapOf(
                    "name" to "Eric Jackin"
                )
            ), Entry(
                sys = EntryMetadata(
                    id = "C2",
                    createdAt = "123",
                    updatedAt = "123",
                    type = "chef",
                    revision = 1
                ),
                fields = mapOf(
                    "name" to "Henrique"
                )
            )
        )
    )

    @Test
    fun `it should return 2 chefs and parse it`() = runBlocking {

        val repository = Mockito.mock(ContentfulNetworkRepository::class.java)

        Mockito.`when`(repository.query("chef")).thenReturn(Response.success(entryCollection))

        val contentfulChefService = ContentfulChefService(repository);

        val chefs = contentfulChefService.getAll()

        assertEquals("Eric Jackin", chefs[0].name)
        assertEquals("Henrique", chefs[1].name)
    }

    @Test
    fun `it should return an empty array in case of null payload`() = runBlocking {

        val repository = Mockito.mock(ContentfulNetworkRepository::class.java)

        Mockito.`when`(repository.query("chef")).thenReturn(Response.success(null))

        val contentfulChefService = ContentfulChefService(repository);

        val chefs = contentfulChefService.getAll()

        assert(chefs.isEmpty())
    }
}
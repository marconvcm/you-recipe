package br.com.htmind.yourecipe.module.inmemory.service

import kotlinx.coroutines.runBlocking
import org.junit.Test

import org.junit.Assert.*

class InMemoryChefServiceTest {

    @Test
    fun getAll() = runBlocking {

        val chefService = InMemoryChefService()

        val chefList = chefService.getAll()

        assertEquals(4, chefList.size)
    }
}
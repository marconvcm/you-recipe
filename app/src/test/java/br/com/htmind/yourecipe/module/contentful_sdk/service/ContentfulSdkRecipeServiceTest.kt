package br.com.htmind.yourecipe.module.contentful_sdk.service

import br.com.htmind.yourecipe.util.ParamsResolver
import com.contentful.java.cda.CDAClient
import kotlinx.coroutines.runBlocking
import org.junit.Test

import org.junit.Assert.*

class ContentfulSdkRecipeServiceTest {

    @Test
    fun getAll() = runBlocking {

        val client = CDAClient.builder().apply {
            setSpace("kk2bw5ojx476")
            setToken("7ac531648a1b5e1dab6c18b0979f822a5aad0fe5f1109829b8a197eb2be4b84c")
        }.build()

        val service = ContentfulSdkRecipeService(client);

        val result = service.getAll()

        assert(result.count() > 0)
    }
}
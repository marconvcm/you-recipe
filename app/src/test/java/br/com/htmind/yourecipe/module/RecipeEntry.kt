package br.com.htmind.yourecipe.module

import br.com.htmind.yourecipe.module.contentful.model.Entry
import br.com.htmind.yourecipe.module.contentful.model.EntryCollection
import br.com.htmind.yourecipe.module.contentful.model.EntryMetadata
import br.com.htmind.yourecipe.module.contentful.model.Includes

object RecipeEntry {
    val entry = EntryCollection(
        total = 1, skip = 0, limit = 1,
        items = listOf(
            Entry(
                sys = EntryMetadata(
                    id = "S1",
                    type = "Dummy",
                    createdAt = "Now",
                    updatedAt = "Now",
                    revision = 1
                ), fields = mapOf(
                    "title" to "Crispy Chicken and Rice\twith Peas & Arugula Salad",
                    "photo" to mapOf(
                        "sys" to mapOf(
                            "type" to "Link",
                            "linkType" to "Asset",
                            "id" to "5mFyTozvSoyE0Mqseoos86"
                        )
                    ),
                    "calories" to 785,
                    "description" to "Crispy chicken skin, tender meat, and rich, tomatoey sauce form a winning trifecta of delicious in this one-pot braise. We spoon it over rice and peas to soak up every last drop of goodness, and serve a tangy arugula salad alongside for a vibrant boost of flavor and color. Dinner is served! Cook, relax, and enjoy!",
                    "chef" to mapOf(
                        "sys" to mapOf(
                            "type" to "Link",
                            "linkType" to "Entry",
                            "id" to "NysGB8obcaQWmq0aQ6qkC"
                        )
                    ),
                    "tags" to arrayOf(
                        mapOf(
                            "sys" to mapOf(
                                "type" to "Link",
                                "linkType" to "Entry",
                                "id" to "61Lgvo6rzUIgIGgcOAMgQ8"
                            )
                        ),
                        mapOf(
                            "sys" to mapOf(
                                "type" to "Link",
                                "linkType" to "Entry",
                                "id" to "gUfhl28dfaeU6wcWSqs0q"
                            )
                        )
                    )
                )
            )
        ), includes = Includes(
            entries = listOf(
                Entry(
                    sys = EntryMetadata(
                        id = "NysGB8obcaQWmq0aQ6qkC",
                        revision = 1,
                        updatedAt = "1",
                        createdAt = "1",
                        type = "Entry"
                    ),
                    fields = mapOf(
                        "name" to "Jony Chives"
                    )
                ),
                Entry(
                    sys = EntryMetadata(
                        id = "61Lgvo6rzUIgIGgcOAMgQ8",
                        revision = 1,
                        updatedAt = "1",
                        createdAt = "1",
                        type = "Entry"
                    ),
                    fields = mapOf(
                        "name" to "gluten free"
                    )
                ),
                Entry(
                    sys = EntryMetadata(
                        id = "61Lgvo6rzUIgIGgcOAM123123gQ8",
                        revision = 1,
                        updatedAt = "1",
                        createdAt = "1",
                        type = "Entry"
                    ),
                    fields = mapOf(
                        "name" to "vegan"
                    )
                ),
                Entry(
                    sys = EntryMetadata(
                        id = "61231231Lgvo6rzUIgIGgcOAM123123gQ8",
                        revision = 1,
                        updatedAt = "1",
                        createdAt = "1",
                        type = "Entry"
                    ),
                    fields = mapOf(
                        "name" to "keto"
                    )
                )
            ),
            assets = listOf(
                Entry(
                    sys = EntryMetadata(
                        id = "5mFyTozvSoyE0Mqseoos86",
                        revision = 1,
                        updatedAt = "1",
                        createdAt = "1",
                        type = "Asset"
                    ),
                    fields = mapOf(
                        "title" to "SKU1479 Hero 077-71d8a07ff8e79abcb0e6c0ebf0f3b69c",
                        "file" to mapOf(
                            "url" to "//images.ctfassets.net/kk2bw5ojx476/5mFyTozvSoyE0Mqseoos86/fb88f4302cfd184492e548cde11a2555/SKU1479_Hero_077-71d8a07ff8e79abcb0e6c0ebf0f3b69c.jpg",
                            "details" to mapOf(
                                "size" to 230068,
                                "image" to mapOf(
                                    "width" to 1020.0,
                                    "height" to 680.0
                                )
                            ),
                            "fileName" to "SKU1479_Hero_077-71d8a07ff8e79abcb0e6c0ebf0f3b69c.jpg",
                            "contentType" to "image/jpeg"
                        )
                    )
                )
            )
        )
    )
}
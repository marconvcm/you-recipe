package br.com.htmind.yourecipe.module.inmemory.service

import br.com.htmind.yourecipe.model.Chef
import br.com.htmind.yourecipe.model.Tag
import br.com.htmind.yourecipe.service.ChefService
import br.com.htmind.yourecipe.service.TagService
import kotlinx.coroutines.runBlocking
import org.junit.Test

import org.junit.Assert.*
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

class InMemoryRecipeServiceTest {

    @Test
    fun getAll() = runBlocking {

        val tagServiceMock = mock(TagService::class.java)
        val chefServiceMock = mock(ChefService::class.java)

        `when`(tagServiceMock.getAll()).thenReturn(arrayOf(Tag(id = "1", name = "Tag A"), Tag(id = "2", name = "Tag B")))

        `when`(chefServiceMock.getAll()).thenReturn(arrayOf(Chef(id = "1", name = "Chef A"), Chef(id = "2", name = "Chef B")))

        val recipeService = InMemoryRecipeService(tagServiceMock, chefServiceMock)

        val recipeList = recipeService.getAll()

        val recipe = recipeList.first()

        assertEquals("Tag A",  recipe.tags.first().name)
        assertEquals("Chef B",  recipe.chef!!.name)
        assertEquals("RC1", recipe.id)
    }
}
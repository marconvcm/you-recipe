package br.com.htmind.yourecipe.module.contentful.model

import br.com.htmind.yourecipe.module.RecipeEntry
import org.junit.Test

import org.junit.Assert.*

class EntryCollectionTest {

    @Test
    fun `it should return entry entity from collection includes`() {

        val include = mapOf( "sys" to mapOf(
            "type" to "Link",
            "linkType" to "Entry",
            "id" to "61Lgvo6rzUIgIGgcOAMgQ8"
        ))

        val entry = RecipeEntry.entry.getEntryFromLink(include)

        assertNotNull(entry)
        assertEquals(entry?.sys?.id, "61Lgvo6rzUIgIGgcOAMgQ8")
        assert(entry!!.fields["name"]!! == "gluten free")
    }

    @Test
    fun `it should return entry asset from collection includes`() {

        val include = mapOf( "sys" to mapOf(
            "type" to "Link",
            "linkType" to "Asset",
            "id" to "5mFyTozvSoyE0Mqseoos86"
        ))

        val entry = RecipeEntry.entry.getAssetFromLink(include)

        val name = entry!!.fields["title"] as String
        assertNotNull(entry)
        assertEquals(entry.sys.id, "5mFyTozvSoyE0Mqseoos86")
        assert(name.startsWith("SKU1479"))
    }

    @Test
    fun `it should return null if does not find the id`() {

        val include = mapOf( "sys" to mapOf(
            "type" to "Link",
            "linkType" to "Asset",
            "id" to "23123"
        ))

        val entry = RecipeEntry.entry.getAssetFromLink(include)
        assertNull(entry)
    }
}
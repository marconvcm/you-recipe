package br.com.htmind.yourecipe.vm

import br.com.htmind.yourecipe.model.Tag
import br.com.htmind.yourecipe.service.ChefService
import br.com.htmind.yourecipe.service.RecipeService
import br.com.htmind.yourecipe.service.TagService
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

import org.junit.Before
import org.mockito.Mockito
import org.mockito.Mockito.mock

class RecipeListViewModelTest {

    @Before
    fun setup(){

    }

    @Test
    fun getTags() = runBlocking {

        val recipeService = mock(RecipeService::class.java)
        val chefService = mock(ChefService::class.java)

        val tagService = mock<TagService> {
                on { getAll() } doReturn GlobalScope.async { arrayOf(
                    Tag(id = "1", name = "tag1"),
                    Tag(id = "2", name = "tag2")
                )
            }
        }

        Mockito.`when`(tagService.getAll()).thenReturn()

        val viewModel = RecipeListViewModel(recipeService, chefService, tagService)

        val result = viewModel.tags.value

        Assert.assertTrue(result!!.isNotEmpty())
    }
}
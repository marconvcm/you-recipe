package br.com.htmind.yourecipe.module.contentful.network

import br.com.htmind.yourecipe.util.ParamsResolver
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.Mockito
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory


class ContentfulRepositoryTest {

    private fun mock(): Pair<ParamsResolver, ContentfulApi> {
        val paramsResolver = Mockito.mock(ParamsResolver::class.java)
        Mockito.`when`(paramsResolver.getString("br.com.htmind.yourecipe.space_id")).thenReturn("kk2bw5ojx476")
        Mockito.`when`(paramsResolver.getString("br.com.htmind.yourecipe.access_token")).thenReturn("7ac531648a1b5e1dab6c18b0979f822a5aad0fe5f1109829b8a197eb2be4b84c")
        Mockito.`when`(paramsResolver.getString("br.com.htmind.yourecipe.environment_id")).thenReturn("master")
        Mockito.`when`(paramsResolver.getString("br.com.htmind.yourecipe.endpoint")).thenReturn("https://cdn.contentful.com")

        val endpoint = paramsResolver.getString("br.com.htmind.yourecipe.endpoint")

        val retrofit = Retrofit.Builder()
            .baseUrl(endpoint)
            .addConverterFactory(MoshiConverterFactory.create())
        .build()

        val api = retrofit.create(ContentfulApi::class.java)
        return Pair(paramsResolver, api)
    }

    @Test
    fun `it should return chef list from network request`() = runBlocking {

        val (paramsResolver, api) = mock()

        val repository = ContentfulRepository(api, paramsResolver)

        val response = repository.query("chef")

        assertEquals(200, response.code())

        val collection = response.body()

        assert(collection?.items?.count() ?: 0 > 0)
        assertEquals(false, collection?.items?.first()?.sys?.id?.isEmpty() ?: true)
    }

    @Test
    fun `it should return chef by id from network request`() = runBlocking {

        val (paramsResolver, api) = mock()

        val repository = ContentfulRepository(api, paramsResolver)

        val response = repository.queryEntry("NysGB8obcaQWmq0aQ6qkC")

        assertEquals(200, response.code())

        val entry = response.body()

        assert(entry?.sys?.id == "NysGB8obcaQWmq0aQ6qkC")
        assert(entry?.fields?.get("name") == "Jony Chives")
    }
}
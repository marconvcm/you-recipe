package br.com.htmind.yourecipe.module.contentful.service

import br.com.htmind.yourecipe.module.contentful.model.Entry
import br.com.htmind.yourecipe.module.contentful.model.EntryCollection
import br.com.htmind.yourecipe.module.contentful.model.EntryMetadata
import br.com.htmind.yourecipe.module.contentful.network.ContentfulNetworkRepository
import kotlinx.coroutines.runBlocking
import org.junit.Test

import org.junit.Assert.*
import org.mockito.Mockito
import retrofit2.Response

class ContentfulTagServiceTest {

    val entryCollection = EntryCollection(
        total = 1,
        skip = 0,
        limit = 10,
        items = listOf(
            Entry(
                sys = EntryMetadata(
                    id = "T1",
                    createdAt = "123",
                    updatedAt = "123",
                    type = "tag",
                    revision = 1
                ),
                fields = mapOf(
                    "name" to "vegan"
                )
            ), Entry(
                sys = EntryMetadata(
                    id = "T2",
                    createdAt = "123",
                    updatedAt = "123",
                    type = "tag",
                    revision = 1
                ),
                fields = mapOf(
                    "name" to "gluten free"
                )
            ), Entry(
                sys = EntryMetadata(
                    id = "T3",
                    createdAt = "123",
                    updatedAt = "123",
                    type = "tag",
                    revision = 1
                ),
                fields = mapOf(
                    "name" to "keto"
                )
            )
        )
    )

    @Test
    fun getAll() = runBlocking {

        val repository = Mockito.mock(ContentfulNetworkRepository::class.java)

        Mockito.`when`(repository.query("tag")).thenReturn(Response.success(entryCollection))

        val tagService = ContentfulTagService(repository);

        val tags = tagService.getAll()

        assertEquals("vegan", tags[0].name)
        assertEquals("T1", tags[0].id)
        assertEquals("gluten free", tags[1].name)
        assertEquals("keto", tags[2].name)
    }
}
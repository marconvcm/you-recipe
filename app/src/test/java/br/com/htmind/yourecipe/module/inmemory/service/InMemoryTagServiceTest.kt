package br.com.htmind.yourecipe.module.inmemory.service

import kotlinx.coroutines.runBlocking
import org.junit.Test

import org.junit.Assert.*

class InMemoryTagServiceTest {

    @Test
    fun getAll() = runBlocking {

        val tagService = InMemoryTagService()

        val tags = tagService.getAll()

        assertEquals(4, tags.size)
    }
}
package br.com.htmind.yourecipe.module.contentful.service

import br.com.htmind.yourecipe.module.RecipeEntry
import br.com.htmind.yourecipe.module.contentful.network.ContentfulNetworkRepository
import kotlinx.coroutines.runBlocking
import org.junit.Test

import org.junit.Assert.*
import org.mockito.Mockito
import retrofit2.Response

class ContentfulRecipeServiceTest {

    @Test
    fun getAll() = runBlocking {

        val repository = Mockito.mock(ContentfulNetworkRepository::class.java)

        Mockito.`when`(repository.query("recipe")).thenReturn(Response.success(RecipeEntry.entry))

        val contentfulRecipeService = ContentfulRecipeService(repository)

        val recipe = contentfulRecipeService.getAll()[0]

        assertEquals("Crispy Chicken and Rice\twith Peas & Arugula Salad", recipe.title)
        assertEquals(2, recipe.tags.size)
        assertNotNull(recipe.chef)
        assertEquals("Jony Chives", recipe.chef!!.name)
        assert(recipe.photo!!.url.startsWith("http"))
    }
}
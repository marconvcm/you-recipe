

# You Recipe

- Open Android Studio and import this project
- You will be able to run this project
- You can run unit test using `./gradlew test`
- App 3 

## Dependencies

- Coroutines
- Koin (Dependency Injector)
- ViewModel (JetPack)
- NavigationController (JetPack)
- Picasso (Image downloader)
- Retrofit (Http)

## Modules

Contentful Module
- This modules connects with Contenful and parse the data for app model

In Memory Module
- This modules uses static hard coded content for debug propose

## Improvements

- Add unit test on ViewModels
- Improve Contenful parse data
- UX and UI improvements (add logo, theme app)
